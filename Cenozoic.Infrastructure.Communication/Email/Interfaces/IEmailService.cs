using System.Threading.Tasks;
namespace Cenozoic.Infrastructure.Communication
{
	public interface IEmailService
	{
		Task Send(string toAddress, string subject, string body, string origin);
	}
}