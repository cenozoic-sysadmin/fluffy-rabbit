using Mandrill;
using Mandrill.Model;
using Microsoft.Extensions.Configuration;
using System.Threading.Tasks;

namespace Cenozoic.Infrastructure.Communication
{
	public class MandrillService : IEmailService
	{

		#region Fields

		private MandrillApi _mandrillApi;
        private readonly IConfiguration _configuration;

		#endregion

		#region Ctor

		public MandrillService(IConfiguration configuration)
		{
            this._configuration = configuration;
			this._mandrillApi = new MandrillApi(_configuration["Mandrill:APIKey"]);
		}

		#endregion

		public async Task Send(string toAddress, string subject, string body, string origin)
		{
			await SendViaMandrill(toAddress, subject, body, origin);
		}

		#region Mandrill

		private async Task SendViaMandrill(string toEmailAddress, string subject, string body, string origin)
		{
			var message = new MandrillMessage(_configuration["Mandrill:FromEmail_" + origin], toEmailAddress, subject, body);
			await _mandrillApi.Messages.SendAsync(message);
		}

		#endregion

	}
}