using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.HttpsPolicy;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.SpaServices.AngularCli;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Design;
using AutoMapper;
using System.IO;
using Cenozoic.Infrastructure;
using Cenozoic.Infrastructure.Communication;
using Cenozoic.Infrastructure.Storage;
using Cenozoic.Infrastructure.Caching;
using Cenozoic.Spatial;
using Singer.Domain;
using Singer.Services;
using Singer.Infrastructure.Data;



namespace Singer.Web
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddMvc().SetCompatibilityVersion(CompatibilityVersion.Version_2_1);

            services.AddAutoMapper();

            // Inject Configuration to other services
            services.AddSingleton<IConfiguration>(Configuration); 

            // In production, the Angular files will be served from this directory
            services.AddSpaStaticFiles(configuration =>
            {
                configuration.RootPath = "ClientApp/dist";
            });

            configureDI(services);
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }
            else
            {
                app.UseExceptionHandler("/Error");
                app.UseHsts();
            }

            app.UseHttpsRedirection();
            app.UseStaticFiles();
            app.UseSpaStaticFiles();

            app.UseMvc(routes =>
            {
                routes.MapRoute(
                    name: "default",
                    template: "{controller}/{action=Index}/{id?}");
            });

            app.UseSpa(spa =>
            {
                // To learn more about options for serving an Angular SPA from ASP.NET Core,
                // see https://go.microsoft.com/fwlink/?linkid=864501

                spa.Options.SourcePath = "ClientApp";

                if (env.IsDevelopment())
                {
                    spa.UseAngularCliServer(npmScript: "start");
                }
            });
        }

        #region DI Containers

        private void configureDI(IServiceCollection services) {
            // Registering DB Context 
            services.AddDbContext<SingerDbContext>(opt => 
                opt.UseSqlServer(Configuration["ConnectionStrings:MainDB"])
            );

            // Registering Repo interface
            // https://github.com/aspnet/EntityFrameworkCore/issues/4558
            services.AddScoped<IWriteEntities>(provider => provider.GetService<SingerDbContext>());

            services.AddScoped<IFreeTextEntities>(provider => provider.GetService<SingerDbContext>());

            // Owner Service
            services.AddScoped<IOwnerService, OwnerService>();

            // Pet Service
            services.AddScoped<IPetService, PetService>();

            // Communication Services
            services.AddScoped<IEmailService, MandrillService>();

            // Storage Service
            services.AddScoped<IStorageService, AzureBlobService>();

            // Caching Service
            services.AddSingleton<ICacheService, MemoryCacheService>();

            // Location Service
            services.AddScoped<ISpatialService, GooglePlaceService>();

            // Contact Us Service
            services.AddScoped<IContactService, ContactService>();
        }

        #endregion
    }
}
