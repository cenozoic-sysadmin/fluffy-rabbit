import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { EffectsModule } from '@ngrx/effects';
import { StoreModule } from '@ngrx/store';
import { RegisterEffects } from './register.effects';
import { RegisterReducer } from './register.reducer';

@NgModule({
  imports: [
    CommonModule,
    StoreModule.forFeature('RegisterState', RegisterReducer),
    EffectsModule.forFeature([RegisterEffects])
  ],
  providers: [RegisterEffects]
})
export class RegisterModule {}
