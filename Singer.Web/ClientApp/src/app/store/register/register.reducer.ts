import { Actions, ActionTypes } from './register.actions';
import { initialState, RegisterState } from './register.state';
import { Pet, Owner } from 'src/app/models/domain.model';

export function RegisterReducer(state = initialState, action: Actions): RegisterState {
  switch (action.type) {
    case ActionTypes.LOAD_REGISTER: {
      return {
        ...state,
        isLoading: true,
      };
    }
    case ActionTypes.ADD_PET: {
      state.owner.Pets.push(new Pet());
      return {
        ...state,
        isLoading: false,
      };
    }
    case ActionTypes.REGISTER: {
      return {
        ...state,
        isLoading: true,
      };
    }
    case ActionTypes.REGISTER_COMPLETE: {
      state.referrals.OwnerToken = action.ownerGUID.item;
      return {
        ...state,
        isLoading: false,
        registered: action.ownerGUID.item != null && action.ownerGUID.item.length > 0,
      };
    }
    case ActionTypes.LOAD_REFER: {
      return {
        ...state,
        isLoading: false,
      };
    }
    case ActionTypes.REFER: {
      return {
        ...state,
        isLoading: true,
      };
    }
    case ActionTypes.REFER_COMPLETE: {
      const newRef = {
        Emails: new Array<string>(3),
        OwnerToken: state.referrals.OwnerToken,
        Origin: document.location.origin
      };
      return {
        ...state,
        isLoading: false,
        referrals: newRef,
        referred: action.success.item
      };
    }
    case ActionTypes.LOAD_DONE: {
      return {
        ...state,
        isLoading: false,
      };
    }
    default: {
      return state;
    }
  }
}
