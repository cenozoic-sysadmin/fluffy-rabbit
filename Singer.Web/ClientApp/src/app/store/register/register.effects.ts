import { Injectable } from '@angular/core';
import { Actions, Effect, ofType } from '@ngrx/effects';
import { Action } from '@ngrx/store';
import { Observable } from 'rxjs';
import { map, switchMap } from 'rxjs/operators';
import { ApiService } from '../../services/api.service';
import * as RegisterActions from './register.actions';
import { RootStoreState } from '../../store';
import { Store } from '@ngrx/store';
import { withLatestFrom, filter } from 'rxjs/operators';
import { getIsValidated, getIsRegistered } from './register.selectors';


@Injectable()
export class RegisterEffects {
  constructor(private api: ApiService, private actions$: Actions, private store$: Store<RootStoreState.State>) {}

  @Effect()
  registerEffect$: Observable<Action> = this.actions$.pipe(
    ofType<RegisterActions.RegisterAction>(
      RegisterActions.ActionTypes.REGISTER
    ),
    withLatestFrom(this.store$),
    filter(actionstate => {
      return getIsValidated(actionstate[1].RegisterState);
    }),
    switchMap(actionstate =>
      this.api.registerOwner(actionstate[1].RegisterState.owner)
      .pipe(
        map(
          item => new RegisterActions.RegisterCompleteAction({item})
        )
      )
     )
  );

  @Effect()
  registerCompleteEffect$: Observable<Action> = this.actions$.pipe(
    ofType<RegisterActions.RegisterCompleteAction>(
      RegisterActions.ActionTypes.REGISTER_COMPLETE
    ),
    withLatestFrom(this.store$),
    filter(actionstate => {
      return getIsRegistered(actionstate[1].RegisterState);
    }),
    map(
      actionstate => new RegisterActions.UploadPetPhotos(actionstate[0].ownerGUID)
    )
  );

  @Effect()
  uploadPetPhotosEffect$: Observable<Action> = this.actions$.pipe(
    ofType<RegisterActions.UploadPetPhotos>(
      RegisterActions.ActionTypes.UPLOAD_PET_PHOTOS
    ),
    withLatestFrom(this.store$),
    filter(actionstate => {
      return getIsRegistered(actionstate[1].RegisterState);
    }),
    switchMap(actionstate =>
      this.api.uploadPetPhotos(actionstate[1].RegisterState.owner, actionstate[0].ownerGUID.item)
      .pipe(
        map(
          item => new RegisterActions.LoadReferAction(actionstate[0].ownerGUID)
        )
      )
     )
  );

  @Effect()
  referEffect$: Observable<Action> = this.actions$.pipe(
    ofType<RegisterActions.ReferAction>(
      RegisterActions.ActionTypes.REFER
    ),
    withLatestFrom(this.store$),
    filter(actionstate => {
      return getIsRegistered(actionstate[1].RegisterState);
    }),
    switchMap(actionstate =>
      this.api.referToFriends(actionstate[1].RegisterState.referrals)
      .pipe(
        map(
          item => new RegisterActions.ReferCompleteAction({item})
        )
      )
     )
  );

}
