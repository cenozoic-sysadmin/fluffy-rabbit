import { createEntityAdapter, EntityAdapter, EntityState } from '@ngrx/entity';
import { Owner, Pet, Referrals } from '../../models/domain.model';
import { selectAllPets } from './register.selectors';

export const featureAdapter: EntityAdapter<Owner> = createEntityAdapter<Owner>({
    selectId: model => model.Id,
    sortComparer: (a: Owner, b: Owner): number =>
    b.Id.toString().localeCompare(a.Id.toString()),
  });

  export interface RegisterState extends EntityState<Owner> {
    owner: Owner;
    referrals: Referrals;
    isLoading?: boolean;
    registered: boolean;
    referred: boolean;
    error?: any;
  }

  export const initialState: RegisterState = featureAdapter.getInitialState(
    {
        owner: new Owner(),
        referrals: new Referrals(),
        detailsValid: false,
        isLoading: false,
        registered: false,
        referred: false,
        error: null
    }
  );
