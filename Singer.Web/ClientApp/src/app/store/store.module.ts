import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { EffectsModule } from '@ngrx/effects';
import { StoreModule } from '@ngrx/store';

import { RegisterModule } from './register/register.module';
import { ContactModule } from './contact/contact.module';

@NgModule({
  imports: [
    CommonModule,
    RegisterModule,
    ContactModule,
    StoreModule.forRoot({}),
    EffectsModule.forRoot([]),
  ],
  declarations: []
})
export class RootStoreModule {}
