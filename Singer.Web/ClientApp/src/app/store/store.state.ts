import { RegisterState } from './register';
import { ContactState } from './contact';

export interface State {
  RegisterState: RegisterState.RegisterState;
  ContactState: ContactState.ContactState;
}
