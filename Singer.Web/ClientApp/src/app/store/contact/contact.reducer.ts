import { Actions, ActionTypes } from './contact.actions';
import { initialState, ContactState } from './contact.state';

export function ContactReducer(state = initialState, action: Actions): ContactState {
  switch (action.type) {
    case ActionTypes.Contact: {
      return {
        ...state,
        isLoading: true,
      };
    }
    case ActionTypes.Contact_COMPLETE: {
      const contRes = {
        FirstName: '',
        LastName: '',
        Email: '',
        Reason: '',
        Message: '',
        Origin: document.location.origin
      };
      return {
        ...state,
        contact: contRes,
        isLoading: false,
        error: action.payload.item
      };
    }
    case ActionTypes.LOAD_DONE: {
      return {
        ...state,
        isLoading: false,
      };
    }
    default: {
      return state;
    }
  }
}
