import { Action } from '@ngrx/store';
import { Contact } from 'src/app/models/domain.model';

export enum ActionTypes {
    Contact = '[Contact] Contact',
    Contact_COMPLETE = '[Contact] Contact Complete',
    LOAD_DONE = '[Contact] Load Done'
}

export type Actions =
    ContactAction | ContactCompleteAction
    | LoadDoneAction;

export class ContactAction implements Action {
  readonly type = ActionTypes.Contact;
  constructor(public payload: {item: Contact}) {}
}

export class ContactCompleteAction implements Action {
  readonly type = ActionTypes.Contact_COMPLETE;
  constructor(public payload: {item: boolean}) {}
}

export class LoadDoneAction implements Action {
  readonly type = ActionTypes.LOAD_DONE;
}
