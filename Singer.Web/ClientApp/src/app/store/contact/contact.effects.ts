import { Injectable } from '@angular/core';
import { Actions, Effect, ofType } from '@ngrx/effects';
import { Action } from '@ngrx/store';
import { Observable } from 'rxjs';
import { map, switchMap } from 'rxjs/operators';
import { ApiService } from 'src/app/services/api.service';
import * as ContactActions from './contact.actions';
import { RootStoreState } from '../../store';
import { Store } from '@ngrx/store';
import { withLatestFrom, filter } from 'rxjs/operators';


@Injectable()
export class ContactEffects {
  constructor(private api: ApiService, private actions$: Actions, private store$: Store<RootStoreState.State>) {}

  @Effect()
  ContactEffect$: Observable<Action> = this.actions$.pipe(
    ofType<ContactActions.ContactAction>(
      ContactActions.ActionTypes.Contact
    ),
    withLatestFrom(this.store$),
    switchMap(actionstate =>
      this.api.submitContactUs(actionstate[0].payload.item)
      .pipe(
        map(
          item => new ContactActions.ContactCompleteAction({item})
        )
      )
     )
  );

  @Effect()
  ContactCompleteEffect$: Observable<Action> = this.actions$.pipe(
    ofType<ContactActions.ContactCompleteAction>(
      ContactActions.ActionTypes.Contact_COMPLETE
    ),
    withLatestFrom(this.store$),
    filter(actionstate => {
      return !actionstate['1'].ContactState.error;
    }),
    map(
      item => new ContactActions.LoadDoneAction()
    )
  );

}
