import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { AboutComponent } from './containers/about/about.component';
import { ContactComponent } from './containers/contact/contact.component';
import { HomeComponent } from './containers/home/home.component';
import { PrivacyComponent } from './containers/privacy/privacy.component';
import { RegisterComponent } from './containers/register/register.component';
import { TermsComponent } from './containers/terms/terms.component';

export function getBaseUrl() {
  return document.getElementsByTagName('base')[0].href;
}

const routes: Routes = [
    { path: '', component: HomeComponent, pathMatch: 'full' },
    { path: 'about', component: AboutComponent },
    { path: 'contact', component: ContactComponent },
    { path: 'privacy', component: PrivacyComponent },
    { path: 'register', component: RegisterComponent },
    { path: 'terms', component: TermsComponent },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule {}
