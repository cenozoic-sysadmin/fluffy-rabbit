import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { AppRoutingModule, getBaseUrl } from './app.routing';
import { FormsModule, ReactiveFormsModule  } from '@angular/forms';
import { HttpClientModule, HttpClient } from '@angular/common/http';
import { JsonpModule, Jsonp, Response } from '@angular/http';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

import { MatDatepickerModule, MatNativeDateModule, MatAutocompleteModule, MatInputModule,
  MatSnackBarModule } from '@angular/material';

import { AppComponent } from './app.component';
import { BtnComponent } from './components/btn/btn.component';
import { DatepickerComponent } from './components/datepicker/datepicker.component';
import { FileuploadComponent } from './components/fileupload/fileupload.component';
import { RadioComponent } from './components/radio/radio.component';
import { DropdownComponent } from './components/dropdown/dropdown.component';
import { FreetextComponent } from './components/freetext/freetext.component';
import { NumericComponent } from './components/numeric/numeric.component';
import { FullAddrComponent } from './components/full-addr/full-addr.component';
import { BreedComponent } from './components/breed/breed.component';
import { PetDetailsComponent } from './components/pet-details/pet-details.component';
import { EmailComponent } from './components/email/email.component';
import { TextareaComponent } from './components/textarea/textarea.component';
import { SnackBarComponent } from './components/snackbar/snackbar.component';
import { NavMenuComponent } from './components/nav-menu/nav-menu.component';
import { NavFooterComponent } from './components/nav-footer/nav-footer.component';
import { SubscribeModalComponent } from './components/subscribe-modal/subscribe-modal.component';

import { AboutComponent } from './containers/about/about.component';
import { ContactComponent } from './containers/contact/contact.component';
import { HomeComponent } from './containers/home/home.component';
import { PrivacyComponent } from './containers/privacy/privacy.component';
import { RegisterComponent } from './containers/register/register.component';
import { RegisterSuccessComponent } from './containers/register-success/register-success.component';
import { TermsComponent } from './containers/terms/terms.component';

import { RootStoreModule } from './store';
import { ApiService } from './services/api.service';

@NgModule({
  declarations: [
    AppComponent,

    BtnComponent,
    DatepickerComponent,
    FileuploadComponent,
    RadioComponent,
    DropdownComponent,
    FreetextComponent,
    NumericComponent,
    FullAddrComponent,
    BreedComponent,
    PetDetailsComponent,
    EmailComponent,
    TextareaComponent,
    SnackBarComponent,
    NavMenuComponent,
    NavFooterComponent,
    SubscribeModalComponent,

    AboutComponent,
    ContactComponent,
    HomeComponent,
    PrivacyComponent,
    RegisterComponent,
    RegisterSuccessComponent,
    TermsComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    FormsModule,
    RootStoreModule,
    HttpClientModule,
    JsonpModule,
    BrowserAnimationsModule,
    ReactiveFormsModule,

    MatDatepickerModule,
    MatNativeDateModule,
    MatAutocompleteModule,
    MatInputModule,
    MatSnackBarModule
  ],
  providers: [
    { provide: 'BASE_URL', useFactory: getBaseUrl, deps: [] },
    HttpClient,
    ApiService,
    MatDatepickerModule,
    MatAutocompleteModule
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
