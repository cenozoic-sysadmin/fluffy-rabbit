import { Injectable, Inject } from '@angular/core';
import { map } from 'rxjs/operators';
import { Observable } from 'rxjs';
import { HttpClient } from '@angular/common/http';
import { Jsonp } from '@angular/http';
import { Owner, GooglePlace, Contact, PetType, Referrals } from '../models/domain.model';

@Injectable()
export class ApiService {
  constructor(
    private http: HttpClient,
    private jsonp: Jsonp,
    @Inject('BASE_URL') private baseURL: string,
  ) {}

  registerOwner(ownerDetails: Owner): Observable<string> {
    return this.http.post<string>(
      `${this.baseURL}api/Register/Register`, ownerDetails
    )
    .pipe(map(result => result));
  }

  uploadPetPhotos(ownerDetails: Owner, ownerGUID: string): Observable<boolean> {
    const formData: FormData = new FormData();
    formData.append('photosDTO.OwnerGUID', ownerGUID);
    let indexcount = 0;
    for (let i = 0; i < ownerDetails.Pets.length; i++) {
      if (ownerDetails.Pets[i].Photo != null) {
        formData.append('photosDTO.PetPhotosIndex[' + indexcount + ']', i.toString());
        indexcount++;
      }
      formData.append('photosDTO.PetPhotos', ownerDetails.Pets[i].Photo);
    }
    return this.http.post<boolean>(
      `${this.baseURL}api/Register/UploadPetPhotos`, formData
    )
    .pipe(map(result => result));
  }

  referToFriends(referrals: Referrals): Observable<boolean> {
    return this.http.post<boolean>(
      `${this.baseURL}api/Register/ReferToFriends`, referrals
    )
    .pipe(map(result => result));
  }

  submitContactUs(contactDetails: Contact): Observable<boolean> {
    return this.http.post<boolean>(
      `${this.baseURL}api/Contact/SubmitContactUs`, contactDetails
    )
    .pipe(map(result => result));
  }

  getAddress(word: string): Observable<GooglePlace[]> {
    return this.http.get<GooglePlace[]>(
      `${this.baseURL}api/Address/Autocomplete?input=` + word,
    )
    .pipe(map(result => result));
  }

  getBreed(word: string, petType: PetType): Observable<string[]> {
    return this.http.get<string[]>(
      `${this.baseURL}api/Pet/GetBreeds?input=` + word + `&petType=` + petType,
    )
    .pipe(map(result => result));
  }

  submitMailChimpMailingList(apiKey: string, mailingListId: string, email: string): boolean {
    let done: boolean;
    this.jsonp.request(
      `https://pet.us19.list-manage.com/subscribe/post-json?u=` + apiKey + `&id=` + mailingListId +
      `&subscribe=Subscribe&EMAIL=` + email + `&c=JSONP_CALLBACK`,
      { method: 'Get'}
    ).subscribe((res) => {
      done = res.ok;
    });
    return done;
  }

}
