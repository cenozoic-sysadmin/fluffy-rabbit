import { Component, OnInit, AfterContentInit, Input, Output, EventEmitter, ViewChild, ElementRef } from '@angular/core';
import { Observable, of } from 'rxjs';

@Component({
  selector: 'app-dropdown',
  templateUrl: './dropdown.component.html',
  styleUrls: ['./dropdown.component.css']
})

export class DropdownComponent implements OnInit, AfterContentInit {
  @Input() disabledOption: string[];
  @Input() selectables: string[][];
  @Input() type: number;
  @Output() selected = new EventEmitter<number>();
  @Output() selectedString = new EventEmitter<string>();
  @ViewChild('self') self: ElementRef;
  value: string;
  currentSelectables$: Observable<string[]>;

  constructor() {}

  ngOnInit() {}

  ngAfterContentInit(): void {
    this.currentSelectables$ = of(this.selectables[0]);
  }

  onSelected(option: string) {
    let elemCount = 0;
    for (let i = 0; i < this.type; i++) {
      elemCount += this.selectables[i].length;
    }
    this.selected.emit(elemCount + this.selectables[this.type].indexOf(option) + 1);
    this.selectedString.emit(option);
    this.value = option;
    this.self.nativeElement.value = option;
  }

  setType(type: number) {
    if (type >= 0 && type <= this.selectables.length - 1) {
      this.type = type;
      this.currentSelectables$ = of(this.selectables[type]);
    }
  }
}
