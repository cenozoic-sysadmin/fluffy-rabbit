import { Component, OnInit, Input, ViewChild} from '@angular/core';
import { Store } from '@ngrx/store';
import { Observable } from 'rxjs';

import { RootStoreState, RegisterSelectors } from 'src/app/store';
import { Pet } from 'src/app/models/domain.model';
import { BreedComponent } from 'src/app/components/breed/breed.component';
import { DropdownComponent } from '../dropdown/dropdown.component';

@Component({
  selector: 'app-pet-details',
  templateUrl: './pet-details.component.html',
  styleUrls: ['./pet-details.component.css']
})
export class PetDetailsComponent implements OnInit {
  @Input() index: number;
  @ViewChild('breed') breed: BreedComponent;
  @ViewChild('bloodType') bloodType: DropdownComponent;
  pet$: Observable<Pet>;

  constructor(private store$: Store<RootStoreState.State>) { }

  ngOnInit() {
    this.pet$ = this.store$.select(
      RegisterSelectors.selectPetByIndex(this.index)
    );
  }

  setPetType(type: number) {
    this.pet$.subscribe(p => p.Type = type);
    this.breed.setPetType(type);
    this.bloodType.setType(type);
  }

  setName(name: string) {
    this.pet$.subscribe(p => p.Name = name);
  }

  setBreed(breed: string) {
    this.pet$.subscribe(p => p.Breed = breed);
  }

  setBloodType(bloodType: number) {
    this.pet$.subscribe(p => p.BloodType = bloodType);
  }

  setPetSize(size: number) {
    this.pet$.subscribe(p => p.Size = size);
  }

  setPetActivityLevel(activityLevel: number) {
    this.pet$.subscribe(p => p.ActivityLevel = activityLevel);
  }

  setWeight(weight: number) {
    this.pet$.subscribe(p => p.Weight = weight);
  }

  setPetGender(gender: number) {
    this.pet$.subscribe(p => p.Gender = gender);
  }

  setAge(age: number) {
    this.pet$.subscribe(p => p.Age = age);
  }

  setAgeUnit(ageUnit: number) {
    this.pet$.subscribe(p => p.AgeUnit = ageUnit);
  }

  setPhoto(photo: File) {
    this.pet$.subscribe(p => p.Photo = photo);
  }

}
