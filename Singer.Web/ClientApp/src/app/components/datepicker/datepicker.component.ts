import { Component, Input, Output, EventEmitter} from '@angular/core';

@Component({
  selector: 'app-datepicker',
  templateUrl: './datepicker.component.html',
  styleUrls: ['./datepicker.component.css']
})

export class DatepickerComponent {
  @Input() placeholder: string;
  @Output() selected = new EventEmitter<Date>();

  public onChange(event: any): void {
    this.selected.emit(event.target.value);
  }

}
