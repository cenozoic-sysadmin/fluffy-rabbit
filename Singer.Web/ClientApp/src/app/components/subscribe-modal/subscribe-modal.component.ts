import { Component, OnInit, Input, ViewChild, ElementRef } from '@angular/core';
import { ApiService } from 'src/app/services/api.service';
import { SnackBarComponent } from 'src/app/components/snackbar/snackbar.component';
import { EmailComponent } from 'src/app/components/email/email.component';

@Component({
  selector: 'app-subscribe-modal',
  templateUrl: 'subscribe-modal.component.html',
  styles: ['./subscribe-modal.component.css'],
})

export class SubscribeModalComponent implements OnInit {
    @Input() modalID: string;
    @ViewChild('snackBar') snackBar: SnackBarComponent;
    @ViewChild('email') email: EmailComponent;
    @ViewChild('modal') modal: ElementRef;
    isValid$: boolean;

    constructor(private api: ApiService) {}

    ngOnInit() {}

    validate(val: string) {
      this.isValid$ = val.length > 0;
    }

    displayInvalidMessage() {
      this.snackBar.open('Please enter your email to subscribe.', 3000, 'sb-alert-warning');
    }

    subscribe(): void {
      this.api.submitMailChimpMailingList('a1309922e68151ba4ddfae429', 'a9afd43053', this.email.value);
      this.modal.nativeElement.click(); // close modal
      this.email.onKeyUp(''); // clear email field
      this.snackBar.open('Thank you for subscribing.', 3000, 'sb-alert-success');
    }

}
