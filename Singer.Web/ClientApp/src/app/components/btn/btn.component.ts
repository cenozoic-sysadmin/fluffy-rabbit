import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-btn',
  templateUrl: './btn.component.html',
  styleUrls: ['./btn.component.css']
})

export class BtnComponent implements OnInit {
    @Input() placeholder: string;
    @Input() classes: string;
    isDisabled = false;

    constructor() {}

    ngOnInit() {}

    disable() {
      this.isDisabled = true;
    }

    enable() {
      if (this.isDisabled) {
        this.isDisabled = false;
      }
    }
}
