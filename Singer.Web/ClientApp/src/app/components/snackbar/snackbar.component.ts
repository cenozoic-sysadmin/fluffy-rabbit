import { Component, ViewEncapsulation } from '@angular/core';
import { MatSnackBar, MatSnackBarConfig} from '@angular/material';

@Component({
  selector: 'app-snack-bar',
  templateUrl: 'snackbar.component.html',
  styleUrls: ['snackbar.component.css'],
  encapsulation: ViewEncapsulation.None
})

export class SnackBarComponent {
    constructor(private snackBar: MatSnackBar) {}

    open(message: string, duration: number, styles: string) {
        const config = new MatSnackBarConfig();
        config.panelClass = [styles];
        config.duration = duration;
        this.snackBar.open(message, '', config);
    }
}
