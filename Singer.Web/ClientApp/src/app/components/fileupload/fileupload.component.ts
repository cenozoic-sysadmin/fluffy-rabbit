import { Component, Input, Output, OnInit, EventEmitter, ViewChild, ElementRef } from '@angular/core';

@Component({
  selector: 'app-fileupload',
  templateUrl: './fileupload.component.html',
  styleUrls: ['./fileupload.component.css']
})

export class FileuploadComponent implements OnInit {
  @Input() placeholder: string;
  @Output() attached = new EventEmitter<File>();
  @ViewChild('imgWrapper') imgWrapper: ElementRef;
  file: File;

  constructor() {}

  ngOnInit() {
  }

  onAttached(files: FileList) {
    this.attached.emit(files.item(0));
    this.file = files.item(0);

    const fr: FileReader = new FileReader();
    fr.readAsDataURL(files[0]);
    fr.onloadend = (data) => {
      if (this.file != null) {
          this.imgWrapper.nativeElement.style = '';
      }
      this.imgWrapper.nativeElement.firstChild.src = fr.result;
    };

  }

}
