export class Owner {
    Id: number;
    Pets: Pet[];
    FirstName: string;
    LastName: string;
    Gender: number;
    DoB: Date;
    Mobile: number;
    Email: string;
    Address: GooglePlace;

    constructor() {
        this.Pets = new Array<Pet>();
        this.Pets.push(new Pet());
        this.DoB = new Date();
        this.Address = new GooglePlace();
        this.Origin = document.location.origin;
    }
    Origin: string;
}

export class Pet {
    Id: number;
    Type: number;
    Name: string;
    Breed: string;
    BloodType: number;
    Size: number;
    ActivityLevel: number;
    Weight: number;
    Gender: number;
    Age: number;
    AgeUnit: number;
    Photo: File;
}

export class Contact {
    FirstName: string;
    LastName: string;
    Email: string;
    Reason: string;
    Message: string;
    Origin: string;
    constructor() {
        this.Origin = document.location.origin;
    }
}

export class Referrals {
    OwnerToken: string;
    Emails: string[];
    Origin: string;

    constructor() {
        this.Emails = new Array<string>();
        this.Origin = document.location.origin;
    }
}

export class GooglePlace {
    spatialId: string;
    fullAddress: string;
}

export enum PetType {
    dog = 0,
    cat = 1
}
