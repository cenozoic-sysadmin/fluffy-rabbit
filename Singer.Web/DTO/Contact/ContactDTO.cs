using System;

namespace Singer.Web
{
    public class ContactDTO
    {
        public string FirstName {get;set;}
        public string LastName {get;set;}
        public string Email {get;set;}
        public string Reason {get;set;}
        public string Message {get;set;}
        public string Origin {get;set;}
    }
}
