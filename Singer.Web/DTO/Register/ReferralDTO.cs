using Singer.Domain;
using System.Collections.Generic;
using System;

namespace Singer.Web
{
    public class ReferralDTO 
    {
        public string OwnerToken {get;set;}
        public List<string> Emails {get;set;}
        public string Origin {get;set;}
    }
}