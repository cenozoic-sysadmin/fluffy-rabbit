using Singer.Domain;
using System.Collections.Generic;
using System;

namespace Singer.Web
{
    public class OwnerDTO 
    {
        public string FirstName {get;set;}
        public string LastName {get;set;}
        public Gender Gender {get;set;}
        public DateTime DoB {get;set;}
        public string Mobile {get;set;}
        public string Email {get;set;}
        public Address Address {get;set;}
        public List<PetDTO> Pets {get;set;}
        public string Token {get;set;}

        public string Origin {get;set;}
    }
}