using Microsoft.AspNetCore.Http;
using Singer.Domain;
using System.Collections.Generic;
using System;

namespace Singer.Web
{
    public class PhotosDTO 
    {
        public string OwnerGUID {get;set;}
        public List<IFormFile> PetPhotos {get;set;}
        public List<string> PetPhotosIndex {get;set;}
    }
}


        