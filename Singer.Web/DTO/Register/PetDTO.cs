using Singer.Domain;
using System.Collections.Generic;
using System;
using Microsoft.AspNetCore.Http;

namespace Singer.Web
{
    public class PetDTO 
    {
        public PetType Type {get;set;}
        public string Name {get;set;}
        public string Breed {get;set;}
        public int BloodType {get;set;}
        public PetSize Size {get;set;}
        public ActivityLevel ActivityLevel {get;set;}
        public decimal Weight {get;set;}
        public Gender Gender {get;set;}
        public decimal Age {get;set;}
        public AgeUnit AgeUnit {get;set;}
    }
}