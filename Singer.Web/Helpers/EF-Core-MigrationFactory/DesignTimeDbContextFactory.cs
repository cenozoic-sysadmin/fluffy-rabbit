using Singer.Infrastructure.Data;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Design;
using Microsoft.Extensions.Configuration;
using System.IO;

public class DesignTimeDbContextFactory : IDesignTimeDbContextFactory<SingerDbContext>
{
    public SingerDbContext CreateDbContext(string[] args)
    {
        IConfigurationRoot Configuration = new ConfigurationBuilder()
            .SetBasePath(Directory.GetCurrentDirectory())
            .AddJsonFile("appsettings.json")
            .Build();
 
        var builder = new DbContextOptionsBuilder<SingerDbContext>();
        var connectionString = Configuration["ConnectionStrings:MainDB"];
 
        builder.UseSqlServer(connectionString);
 
        return new SingerDbContext(builder.Options);
    }
}