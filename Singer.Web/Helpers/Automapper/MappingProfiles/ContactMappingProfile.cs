using AutoMapper;
using System.Collections.Generic;
using System.Linq;
using Singer.Domain;
using Singer.Services;
 
namespace Singer.Web
{
    public class ContactMappingProfile : Profile
    {
        public ContactMappingProfile()
        {
            applyDTOToDomainMappingRules();
        }
        private void applyDTOToDomainMappingRules()
        {
            CreateMap<ContactDTO, ContactEnquiry>()
                .ForMember(dest => dest.Id, opt => opt.Ignore())
                .ForMember(dest => dest.CreatedAt, opt => opt.Ignore())
            ;
        }
    }
  
}