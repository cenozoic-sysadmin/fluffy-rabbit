using AutoMapper;
using System.Collections.Generic;
using System.Linq;
using Singer.Domain;
using Singer.Services;
 
namespace Singer.Web
{
    public class PetMappingProfile : Profile
    {
        public PetMappingProfile()
        {
            applyDTOToDomainMappingRules();
        }
        private void applyDTOToDomainMappingRules()
        {
            CreateMap<PetDTO, Pet>()
                .ForMember(dest => dest.Id, opt => opt.Ignore())
                .ForMember(dest => dest.PetBreeds, opt => opt.Ignore())
                .ForMember(dest => dest.BloodType, opt => opt.Ignore())
                    .AfterMap<PetMappingAction>()
            ;
        }

    }
  
}