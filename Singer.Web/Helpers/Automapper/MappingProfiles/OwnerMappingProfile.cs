using AutoMapper;
using System.Collections.Generic;
using System.Linq;
using Singer.Domain;
using Singer.Services;
 
namespace Singer.Web
{
    public class OwnerMappingProfile : Profile
    {
        public OwnerMappingProfile()
        {
            applyDTOToDomainMappingRules();
        }
        private void applyDTOToDomainMappingRules()
        {
            CreateMap<OwnerDTO, Owner>()
                .ForMember(dest => dest.Id, opt => opt.Ignore())
                .ForMember(dest => dest.Pets, opt => opt.Ignore())
                .ForMember(dest => dest.Token, opt => opt.Ignore())
                    .AfterMap<OwnerMappingAction>()
            ;
        }
    }
  
}