using AutoMapper;
using System;
using System.Linq;
using System.Collections.Generic;
using Singer.Domain;
using Singer.Services;

namespace Singer.Web
{
    public class PetMappingAction : IMappingAction<PetDTO, Pet>
    {
        private readonly IPetService _petServices;

        public PetMappingAction(IPetService petServices)
        {
            this._petServices = petServices;
        }

        public void Process(PetDTO src, Pet dest)
        {
            Breed breed;
            if (string.IsNullOrEmpty(src.Breed)) {
                var breeds = _petServices.RetrieveBreeds(src.Type);
                breed = breeds.First();
            } else {
                breed = _petServices.RetrieveBreed(src.Breed, src.Type);
                if (breed == null) // Create Breed name if not exists as per requirement
                    breed = _petServices.CreateBreed(src.Breed, src.Type);
            }
            dest.AssignBreed(breed);

            var bloodType = _petServices.RetrieveBloodType(src.BloodType);
            if (bloodType == null) // Defaults to first blood type if doesn't exists
                bloodType = _petServices.RetrieveBloodTypes(src.Type).First();
            dest.AssignBloodType(bloodType);

            var multiplier = src.AgeUnit == AgeUnit.Year ? 12 : 1;
            dest.DoB = DateTime.Now.AddMonths(-(Decimal.ToInt32(src.Age*multiplier)));
        }
    }
}