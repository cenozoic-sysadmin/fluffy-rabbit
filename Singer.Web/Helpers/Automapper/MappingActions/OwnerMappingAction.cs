using AutoMapper;
using System.Collections.Generic;
using System.Linq;
using System;
using System.IO;
using Singer.Domain;
using Singer.Services;
 
namespace Singer.Web
{
    public class OwnerMappingAction : IMappingAction<OwnerDTO, Owner>
    {
        private readonly IMapper _mapper;

        public OwnerMappingAction(IMapper mapper)
        {
            this._mapper = mapper;
        }

        public void Process(OwnerDTO src, Owner dest)
        {
            src.Pets.ForEach(petDTO => {
                try {
                    var pet = _mapper.Map<Pet>(petDTO);
                    dest.AssignPet(pet);
                } catch (Exception ex) {
                    Console.WriteLine(ex.Message);
                }
            });
        }
    }
}