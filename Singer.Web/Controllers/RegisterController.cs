using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Http;
using Cenozoic.Infrastructure;
using Cenozoic.Infrastructure.Storage;
using Singer.Domain;
using Singer.Services;
using AutoMapper;

namespace Singer.Web.Controllers
{
    [Route("api/[controller]")]
    public class RegisterController : Controller
    {
        private readonly IMapper _mapper;
        private readonly IOwnerService _ownerServices;
        private readonly IStorageService _storageServices;

        public RegisterController(IMapper mapper, IOwnerService ownerServices, IStorageService storageServices)
        {
            this._mapper = mapper;
            this._ownerServices = ownerServices;
            this._storageServices = storageServices;
        }

        #region APIs

        [HttpPost("[action]")]
        public Guid Register([FromBody]OwnerDTO ownerDTO)
        {
            var owner = _mapper.Map<Owner>(ownerDTO);
            owner = _ownerServices.CreateOwner(owner);
            return owner == null ? new Guid() : owner.Token;
        }

        [HttpPost("[action]")]
        public async Task UploadPetPhotos([FromForm]PhotosDTO photosDTO)
        {
            var owner = _ownerServices.RetrieveOwner(new Guid(photosDTO.OwnerGUID));
            if (owner == null)
                return;
            // add/replace photos based on pet order
            var pets = _ownerServices.RetrievePets(owner.Id);
            for(var i = 0; i < photosDTO.PetPhotos.Count; i++)
            {
                int petIndex; Int32.TryParse(photosDTO.PetPhotosIndex[i], out petIndex);
                var pet = pets.ElementAt(petIndex);
                var thisPet = _ownerServices.RetrievePet(owner.Id, pet.Id);

                var profileImg = photosDTO.PetPhotos.ElementAt(i);
                var bloburl = await _storageServices.UploadToStorage(profileImg, photosDTO.OwnerGUID);

                thisPet.AssignProfilePicture(bloburl);
                _ownerServices.UpdatePet(owner.Id, thisPet);
            }
        }


        [HttpPost("[action]")]
        public async Task<bool> ReferToFriends([FromBody]ReferralDTO referrals)
        {
            if (referrals == null || referrals.Emails == null || referrals.OwnerToken == null)
                return false;

            var owner = _ownerServices.RetrieveOwner(new Guid(referrals.OwnerToken));
            var origin = referrals.Origin.Contains("drool") ? "Drool" : "Singr";
            if (owner == null)
                return false;
            var html = await this.RenderViewAsync("~/EmailTemplates/Referrals/Mandrill_Invite_Friends_" + origin + ".cshtml", owner, false);
            return await _ownerServices.ReferToFriends(owner.Id, referrals.Emails, html, origin);
        }

        #endregion

    }
}
