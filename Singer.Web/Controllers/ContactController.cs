using AutoMapper;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Singer.Services;
using Singer.Domain;

namespace Singer.Web
{
    [Route("api/[controller]")]
    public class ContactController : Controller
    {
        private readonly IContactService _contactServices;
        private readonly IMapper _mapper;

        public ContactController(IContactService contactServices, IMapper mapper)
        {
            this._contactServices = contactServices;
            this._mapper = mapper;
        }

        [HttpPost("[action]")]
        public async Task<bool> SubmitContactUs([FromBody] ContactDTO contactDetails)
        {
            var contactEnquiry = _mapper.Map<ContactEnquiry>(contactDetails);
            var origin = contactDetails.Origin.Contains("drool") ? "Drool" : "Singr";
            var html = await this.RenderViewAsync("~/EmailTemplates/Contact/Mandrill_Contact_Confirmation_" + origin + ".cshtml", contactEnquiry, false);
            return await _contactServices.SaveContactDetailsAndConfirm(contactEnquiry, html, origin);
        }
    }
}