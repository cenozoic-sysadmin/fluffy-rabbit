using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Singer.Services;
using Singer.Domain;
using System.Linq;


namespace Singer.Web.Controllers
{
    [Route("api/[controller]")]
    public class PetController : Controller
    {
        private readonly IPetService _petServices;

        public PetController(IPetService petServices)
        {
            this._petServices = petServices;
        }

        #region APIs
        [HttpGet("[action]")]
        public async Task<List<string>> GetBreeds(string input, PetType petType)
        {
            if (string.IsNullOrEmpty(input) || petType < 0)
                return new List<string>();
            var res = await _petServices.SearchBreed(input, petType);
            return res.Select(b => b.Name).ToList();
        }
        #endregion

    }
}
