using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Cenozoic.Spatial;


namespace Singer.Web.Controllers
{
    [Route("api/[controller]")]
    public class AddressController : Controller
    {
        private readonly ISpatialService _spatialServices;

        public AddressController(ISpatialService spatialService)
        {
            this._spatialServices = spatialService;
        }

        #region APIs
        [HttpGet("[action]")]
        public async Task<List<AutoCompleteDetails>> Autocomplete(string input)
        {
            return await _spatialServices.GetSpatialAutoComplete(input);
        }
        #endregion

    }
}
