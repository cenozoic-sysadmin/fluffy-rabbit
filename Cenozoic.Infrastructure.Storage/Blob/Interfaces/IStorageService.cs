using System.IO;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;

namespace Cenozoic.Infrastructure.Storage
{
	public interface IStorageService
	{
		Task<string> UploadToStorage(IFormFile file, string containerName);
		Task<bool> DeleteFromStorage(string fileURL, string containerName);
	}
}