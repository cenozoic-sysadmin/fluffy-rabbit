using Microsoft.WindowsAzure.Storage;
using Microsoft.WindowsAzure.Storage.Blob;
using Microsoft.Extensions.Configuration;
using Microsoft.AspNetCore.Http;
using System;
using System.IO;
using System.Threading.Tasks;
using System.Security.Cryptography;
using System.Web;

namespace Cenozoic.Infrastructure.Storage
{
	public class AzureBlobService : IStorageService
	{

		#region Fields

		private readonly IConfiguration _configuration;

		#endregion

		#region Ctor

		public AzureBlobService(IConfiguration configuration)
		{
			this._configuration = configuration;
		}

		#endregion

		#region IUploadService

		public async Task<string> UploadToStorage(IFormFile file, string containerName)
		{
			// https://docs.microsoft.com/en-us/azure/storage/blobs/storage-dotnet-how-to-use-blobs
			CloudStorageAccount storageAccount = CloudStorageAccount.Parse(_configuration["Azure:BlobConnectionString"]);
			CloudBlobClient blobClient = storageAccount.CreateCloudBlobClient();
			CloudBlobContainer container = blobClient.GetContainerReference(containerName);
			await container.CreateIfNotExistsAsync();
			await container.SetPermissionsAsync(new BlobContainerPermissions { PublicAccess = BlobContainerPublicAccessType.Blob });

			// Retrieve reference to a blob
			CloudBlockBlob blockBlob = container.GetBlockBlobReference(getNewFileName(file));

			// Set mime type if it's an image
			if (file.ContentType.ToLower().Contains("image"))
				blockBlob.Properties.ContentType = file.ContentType.ToLower();

			// Reset the Stream to the beginning and upload
			file.OpenReadStream().Seek(0, SeekOrigin.Begin);
			await blockBlob.UploadFromStreamAsync(file.OpenReadStream());

			return blockBlob.Uri.ToString();
		}

		public async Task<bool> DeleteFromStorage(string fileURL, string containerName)
		{
			if (String.IsNullOrEmpty(fileURL))
				return false;
			int position = fileURL.IndexOf(containerName) + containerName.Length + 1;
			string fileName = fileURL.Substring(position);
			// Retrieve storage account from connection string.
			CloudStorageAccount storageAccount = CloudStorageAccount.Parse(
					_configuration["Azure:BlobConnectionString"]);

			// Create the blob client.
			CloudBlobClient blobClient = storageAccount.CreateCloudBlobClient();

			// Retrieve reference to a previously created container.
			CloudBlobContainer container = blobClient.GetContainerReference(containerName);

			// Retrieve reference to a blob named "myblob.txt".
			CloudBlockBlob blockBlob = container.GetBlockBlobReference(fileName);

			if (await blockBlob.ExistsAsync())
			{
				await blockBlob.DeleteAsync();
				return true;
			}
			return false;
		}

		#endregion

		#region Helper

		private string getNewFileName(IFormFile file)
		{
			// 	string mimeType = file.ContentType.ToLower();   //mimeType == "image/png", or "application/pdf"
			MD5 md5 = MD5.Create();
			var hashMD5 = md5.ComputeHash(file.OpenReadStream());
			//For new file name prefix
			string fileNamePrefix = Guid.NewGuid().ToString();
			string fileMD5String = BitConverter.ToString(hashMD5).Replace("-", string.Empty);
			string path = fileMD5String.Substring(0, 3);  //sub directory
			string fileName = file.FileName.ToLower();
			//Use for long file name, cut to first 20 characters
			if (fileName.Length > 20)
			{
				var fileType = fileName.Substring(fileName.LastIndexOf('.'));
				fileName = fileName.Substring(0, 20) + fileType;
			}
			string newFileName = path + "/" + fileNamePrefix + "-" + fileName;
			return newFileName;
		}

		private string GetBlobSasUri(CloudBlockBlob blob)
		{
			//Set the expiry time and permissions for the blob.
			//In this case the start time is specified as a few minutes in the past, to mitigate clock skew.
			//The shared access signature will be valid immediately.
			SharedAccessBlobPolicy sasConstraints = new SharedAccessBlobPolicy();
			sasConstraints.SharedAccessStartTime = DateTime.UtcNow.AddMinutes(-5);
			sasConstraints.SharedAccessExpiryTime = DateTime.UtcNow.AddHours(24);
			sasConstraints.Permissions = SharedAccessBlobPermissions.Read | SharedAccessBlobPermissions.Write;

			//Generate the shared access signature on the blob, setting the constraints directly on the signature.
			string sasBlobToken = blob.GetSharedAccessSignature(sasConstraints);

			//Return the URI string for the container, including the SAS token.
			return blob.Uri + sasBlobToken;
		}

		#endregion

	}
}