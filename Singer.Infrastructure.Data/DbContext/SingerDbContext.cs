using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using System.Linq;
using System.Linq.Expressions;
using Microsoft.EntityFrameworkCore;

using Singer.Domain;
using Cenozoic.Infrastructure;

namespace Singer.Infrastructure.Data
{
    public class SingerDbContext : DbContext, IWriteEntities, IFreeTextEntities
    {
        public SingerDbContext(DbContextOptions<SingerDbContext> options) : base(options) { }
        
        #region DbSets
        
        public virtual DbSet<Owner> Owners {get;set;}
        public virtual DbSet<Pet> Pets {get;set;}
        public virtual DbSet<Breed> Breeds {get;set;}
        
        #endregion

        #region OnModelCreating

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            // EF core 2.0+ does not support scaffolding value object without explicit configurations
            // https://docs.microsoft.com/en-us/ef/core/modeling/owned-entities
            modelBuilder.Entity<Owner>().OwnsOne(o => o.Address);
			modelBuilder.Entity<Breed>()
				.Property(b => b.Name)
				.IsRequired();
			modelBuilder.Entity<ContactEnquiry>();
        }

        #endregion

        #region Repository Implementations

		// TODO: Move interface to IReadEntities 
		public async Task<List<TEntity>> FreeTextSearch<TEntity>(string searchText, PetType petType) where TEntity : Breed
		{
			// return this.Set<TEntity>().Where(b => EF.Functions.FreeText(b.Name, searchText)).ToList();
			// Will use wildcard search for autocomplete implemetation
			return await this.Set<TEntity>().Where(b => b.Name.Contains(searchText) && b.PetType == petType && b.Approved).ToListAsync();
		}

        public List<TEntity> SqlQuery<TEntity>(string sql, params object[] parameters) where TEntity : class
		{
            // https://docs.microsoft.com/en-us/ef/core/querying/raw-sql
			return this.Set<TEntity>().FromSql<TEntity>(sql, parameters).ToList<TEntity>();
		}

		public TEntity GetById<TEntity>(object id) where TEntity : class
		{
			if (id == null)
				throw new ArgumentNullException("id");

			return this.Set<TEntity>().Find(id);
		}

		public async Task<TEntity> GetByIdAsync<TEntity>(object id) where TEntity : class
		{
			if (id == null)
				throw new ArgumentNullException("id");

			return await this.Set<TEntity>().FindAsync(id);
		}

		public IEnumerable<TEntity> Get<TEntity>(
			Expression<Func<TEntity, bool>> filter = null,
			Func<IQueryable<TEntity>, IOrderedQueryable<TEntity>> orderBy = null,
			Expression<Func<TEntity, object>>[] includeProperties = null,
			int? skip = null,
			int? take = null)
			where TEntity : class
		{
			return this.GetQueryable(filter, orderBy, includeProperties, skip, take).ToList();
		}

		public async Task<IEnumerable<TEntity>> GetAsync<TEntity>(
			Expression<Func<TEntity, bool>> filter = null,
			Func<IQueryable<TEntity>, IOrderedQueryable<TEntity>> orderBy = null,
			Expression<Func<TEntity, object>>[] includeProperties = null,
			int? skip = null,
			int? take = null)
			where TEntity : class
		{
			return await this.GetQueryable(filter, orderBy, includeProperties, skip, take).ToListAsync();
		}

		public IQueryable<TEntity> GetLazy<TEntity>(
			Expression<Func<TEntity, bool>> filter = null,
			Func<IQueryable<TEntity>, IOrderedQueryable<TEntity>> orderBy = null,
			Expression<Func<TEntity, object>>[] includeProperties = null,
			int? skip = null,
			int? take = null)
			where TEntity : class
		{
			return this.GetQueryable(filter, orderBy, includeProperties, skip, take);
		}

		public IQueryable<TEntity> GetLazyNoTracking<TEntity>(
			Expression<Func<TEntity, bool>> filter = null,
			Func<IQueryable<TEntity>, IOrderedQueryable<TEntity>> orderBy = null,
			Expression<Func<TEntity, object>>[] includeProperties = null,
			int? skip = null,
			int? take = null)
			where TEntity : class
		{
			return this.GetQueryable(filter, orderBy, includeProperties, skip, take).AsNoTracking();
		}

		public TEntity Single<TEntity>(
			Expression<Func<TEntity, bool>> filter = null,
			Expression<Func<TEntity, object>>[] includeProperties = null)
			where TEntity : class
		{
			return this.GetQueryable(filter, null, includeProperties).SingleOrDefault();
		}

		public async Task<TEntity> SingleAsync<TEntity>(
			Expression<Func<TEntity, bool>> filter = null,
			Expression<Func<TEntity, object>>[] includeProperties = null)
			where TEntity : class
		{
			return await this.GetQueryable(filter, null, includeProperties).SingleOrDefaultAsync();
		}

		public TEntity First<TEntity>(
			Expression<Func<TEntity, bool>> filter = null,
			Expression<Func<TEntity, object>>[] includeProperties = null)
			where TEntity : class
		{
			return this.GetQueryable(filter, null, includeProperties).FirstOrDefault();
		}

		public async Task<TEntity> FirstAsync<TEntity>(
			Expression<Func<TEntity, bool>> filter = null,
			Expression<Func<TEntity, object>>[] includeProperties = null)
			where TEntity : class
		{
			return await this.GetQueryable(filter, null, includeProperties).FirstOrDefaultAsync();
		}

		public int Count<TEntity>(Expression<Func<TEntity, bool>> filter = null) where TEntity : class
		{
			return this.GetQueryable(filter).Count();
		}

		public async Task<int> CountAsync<TEntity>(Expression<Func<TEntity, bool>> filter = null) where TEntity : class
		{
			return await this.GetQueryable(filter).CountAsync();
		}

		public bool Exists<TEntity>(Expression<Func<TEntity, bool>> filter = null) where TEntity : class
		{
			return this.GetQueryable(filter).Any();
		}

		public async Task<bool> ExistsAsync<TEntity>(Expression<Func<TEntity, bool>> filter = null) where TEntity : class
		{
			return await this.GetQueryable(filter).AnyAsync();
		}

		public void Create<TEntity>(TEntity entity) where TEntity : class
		{
			if (this.Entry(entity).State == EntityState.Detached)
				this.Set<TEntity>().Add(entity);
		}

		public new void Update<TEntity>(TEntity entity) where TEntity : class
		{
			if (this.Entry(entity).State == EntityState.Detached)
				this.Set<TEntity>().Attach(entity);

			this.Entry(entity).State = EntityState.Modified;
		}

		public void Delete<TEntity>(TEntity entity) where TEntity : class
		{
			if (this.Entry(entity).State == EntityState.Detached)
				this.Set<TEntity>().Attach(entity);

			this.Set<TEntity>().Remove(entity);
		}

		public void Save()
		{
			//try
			//{
				this.SaveChanges();
			//}
			//catch (DbEntityValidationException e)
			//{
			//	ThrowEnhancedValidationException(e);
			//}
		}

		public Task SaveAsync()
		{
			//try
			//{
				return this.SaveChangesAsync();
			//}
			//catch (Exception e)
			//{
			//	ThrowEnhancedValidationException(e);
			//}

			//return Task.CompletedTask;
		}

		#endregion

		#region Helpers

		private IQueryable<TEntity> GetQueryable<TEntity>(
			Expression<Func<TEntity, bool>> filter = null,
			Func<IQueryable<TEntity>, IOrderedQueryable<TEntity>> orderBy = null,
			Expression<Func<TEntity, object>>[] includeProperties = null,
			int? skip = null,
			int? take = null)
			where TEntity : class
		{
			IQueryable<TEntity> query = this.Set<TEntity>();

			if (filter != null)
				query = query.Where(filter);

			if (orderBy != null)
				query = orderBy(query);

			if (includeProperties != null)
			{
				foreach (var prop in includeProperties)
					if (prop != null)
						query.Include(prop);
			}

			if (skip.HasValue)
				query = query.Skip(skip.Value);

			if (take.HasValue)
				query = query.Take(take.Value);

			return query;
		}

        // EF-core validation removed
        // https://www.bricelam.net/2016/12/13/validation-in-efcore.html
		/*
        private void ThrowEnhancedValidationException(DbEntityValidationException e)
		{
			var errorMessages = e.EntityValidationErrors
														.SelectMany(x => x.ValidationErrors)
														.Select(x => x.ErrorMessage);

			var fullErrorMessage = string.Join("; ", errorMessages);
			var exceptionMessage = string.Concat(e.Message, " The validation errors are: ", fullErrorMessage);

			throw new ValidationException(exceptionMessage, e.EntityValidationErrors);
		}
        */

        #endregion


    }
}
