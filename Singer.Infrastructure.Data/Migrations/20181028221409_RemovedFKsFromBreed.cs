﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Singer.Infrastructure.Data.Migrations
{
    public partial class RemovedFKsFromBreed : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_PetBreed_Breed_BreedId",
                table: "PetBreed");

            migrationBuilder.DropIndex(
                name: "IX_PetBreed_BreedId",
                table: "PetBreed");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateIndex(
                name: "IX_PetBreed_BreedId",
                table: "PetBreed",
                column: "BreedId");

            migrationBuilder.AddForeignKey(
                name: "FK_PetBreed_Breed_BreedId",
                table: "PetBreed",
                column: "BreedId",
                principalTable: "Breed",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }
    }
}
