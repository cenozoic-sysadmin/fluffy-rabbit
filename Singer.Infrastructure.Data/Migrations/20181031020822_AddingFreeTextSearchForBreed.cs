﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Singer.Infrastructure.Data.Migrations
{
    public partial class AddingFreeTextSearchForBreed : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.
                Sql("CREATE FULLTEXT CATALOG breed_ft_catalogue AS DEFAULT", true);
            migrationBuilder.                
                Sql(
                    @"CREATE FULLTEXT INDEX ON dbo.Breed(Name) KEY INDEX PK_Breed ON breed_ft_catalogue"
                , true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.Sql("DROP FULLTEXT INDEX ON dbo.Breed");
            migrationBuilder.Sql("DROP FULLTEXT CATALOG breed_ft_catalogue");
        }
    }
}
