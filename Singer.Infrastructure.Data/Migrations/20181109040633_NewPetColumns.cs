﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace Singer.Infrastructure.Data.Migrations
{
    public partial class NewPetColumns : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "Age",
                table: "Pet");

            migrationBuilder.AlterColumn<int>(
                name: "Size",
                table: "Pet",
                nullable: false,
                oldClrType: typeof(float));

            migrationBuilder.AddColumn<int>(
                name: "ActivityLevel",
                table: "Pet",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddColumn<DateTime>(
                name: "DoB",
                table: "Pet",
                nullable: false,
                defaultValue: new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified));

            migrationBuilder.AddColumn<string>(
                name: "Address_SpatialId",
                table: "Owner",
                nullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "ActivityLevel",
                table: "Pet");

            migrationBuilder.DropColumn(
                name: "DoB",
                table: "Pet");

            migrationBuilder.DropColumn(
                name: "Address_SpatialId",
                table: "Owner");

            migrationBuilder.AlterColumn<float>(
                name: "Size",
                table: "Pet",
                nullable: false,
                oldClrType: typeof(int));

            migrationBuilder.AddColumn<float>(
                name: "Age",
                table: "Pet",
                nullable: false,
                defaultValue: 0f);
        }
    }
}
