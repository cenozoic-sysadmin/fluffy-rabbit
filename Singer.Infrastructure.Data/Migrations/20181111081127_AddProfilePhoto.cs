﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Singer.Infrastructure.Data.Migrations
{
    public partial class AddProfilePhoto : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<string>(
                name: "ProfilePicture",
                table: "Pet",
                nullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "ProfilePicture",
                table: "Pet");
        }
    }
}
