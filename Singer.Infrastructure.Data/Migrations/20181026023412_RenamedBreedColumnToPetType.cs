﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Singer.Infrastructure.Data.Migrations
{
    public partial class RenamedBreedColumnToPetType : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "Type",
                table: "Breed");

            migrationBuilder.AddColumn<int>(
                name: "PetType",
                table: "Breed",
                nullable: false,
                defaultValue: 0);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "PetType",
                table: "Breed");

            migrationBuilder.AddColumn<int>(
                name: "Type",
                table: "Breed",
                nullable: false,
                defaultValue: 0);
        }
    }
}
