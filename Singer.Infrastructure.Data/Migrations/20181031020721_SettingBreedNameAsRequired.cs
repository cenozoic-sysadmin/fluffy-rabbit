﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Singer.Infrastructure.Data.Migrations
{
    public partial class SettingBreedNameAsRequired : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AlterColumn<string>(
                name: "Name",
                table: "Breed",
                nullable: false,
                oldClrType: typeof(string),
                oldNullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AlterColumn<string>(
                name: "Name",
                table: "Breed",
                nullable: true,
                oldClrType: typeof(string));
        }
    }
}
