﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Singer.Infrastructure.Data.Migrations
{
    public partial class AddedOriginFlagsForOwnerAndContact : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<string>(
                name: "Origin",
                table: "Owner",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "Origin",
                table: "ContactEnquiry",
                nullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "Origin",
                table: "Owner");

            migrationBuilder.DropColumn(
                name: "Origin",
                table: "ContactEnquiry");
        }
    }
}
