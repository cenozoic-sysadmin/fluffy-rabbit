using Singer.Domain;
using System.Threading.Tasks;
using System.Collections.Generic;

namespace Singer.Infrastructure.Data
{
    public interface IFreeTextEntities
    {
        Task<List<TEntity>> FreeTextSearch<TEntity>(string searchText, PetType petType) where TEntity : Breed;
    }
}