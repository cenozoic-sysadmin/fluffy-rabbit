using System.Threading.Tasks;

namespace Cenozoic.Infrastructure
{
	public interface IUnitOfWork
	{
		void Save();
		Task SaveAsync();
	}
}