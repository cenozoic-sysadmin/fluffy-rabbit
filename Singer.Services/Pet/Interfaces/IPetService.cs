using Singer.Domain;
using System.Threading.Tasks;
using System.Collections.Generic;


namespace Singer.Services 
{
    public interface IPetService
    {
        Breed CreateBreed(string breedName, PetType petType);
        Breed RetrieveBreed(int breedId);
        Breed RetrieveBreed(string breedName, PetType petType);
        IEnumerable<Breed> RetrieveBreeds(PetType petType);
        Breed UpdateBreed(int breedId, Breed breedData);
        Task<List<Breed>> SearchBreed(string input, PetType petType);

        BloodType CreateBloodType(string bloodTypeName, PetType petType);
        BloodType RetrieveBloodType(int bloodTypeId);
        BloodType RetrieveBloodType(string bloodTypeName, PetType petType);
        List<BloodType> RetrieveBloodTypes(PetType petType);
        BloodType UpdateBloodType(int bloodTypeId, BloodType bloodTypeData);
    }
}