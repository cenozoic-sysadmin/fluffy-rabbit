using AutoMapper;
using Cenozoic.Infrastructure;
using Cenozoic.Infrastructure.Caching;
using System;
using System.Linq;
using System.Collections.Generic;
using System.Threading.Tasks;
using Singer.Domain;
using Singer.Infrastructure.Data;


namespace Singer.Services
{
    public class PetService : IPetService
    {
        private readonly IWriteEntities _entities;
        private readonly IFreeTextEntities _freeTextEntities;
        private readonly IMapper _mapper;
        private readonly ICacheService _cacheServices;

        public PetService(IWriteEntities entities, IMapper mapper, IFreeTextEntities freeTextEntities, ICacheService cacheServices)
        {
            this._entities = entities;
            this._mapper = mapper;
            this._freeTextEntities = freeTextEntities;
            this._cacheServices = cacheServices;
        }

        #region Breeds
        public Breed CreateBreed(string name, PetType petType)
        {
            var breed = RetrieveBreed(name, petType);
            if (breed != null)
                return breed;

            breed = new Breed(name, petType);
            saveAs(breed);
            return breed;
        }
        public Breed RetrieveBreed(int breedId)
        {
            return _entities.Single<Breed>(b => b.Id == breedId);
        }
        public IEnumerable<Breed> RetrieveBreeds(PetType petType)
        {
            return _entities.Get<Breed>(b => b.PetType == petType);
        }
        public Breed RetrieveBreed(string breedName, PetType petType)
        {
            return _entities.Single<Breed>(b => b.Name == breedName && b.PetType == petType);
        }
        public Breed UpdateBreed(int breedId, Breed breedData)
        {
            if (breedExists(breedId))
            {
                var existingBreed = RetrieveBreed(breedId);
                existingBreed = _mapper.Map<Breed, Breed>(breedData, existingBreed);
                save(existingBreed);
                return existingBreed;
            }
            return null;
        }

        public async Task<List<Breed>> SearchBreed(string input, PetType petType)
        {
            return await _cacheServices.GetOrAddAsync("Breed_" + input + "_" + petType.ToString(), () => this._freeTextEntities.FreeTextSearch<Breed>(input, petType), DateTimeOffset.Now.AddDays(1), null, null);
        }

        #endregion

        #region Blood Types

        public BloodType CreateBloodType(string name, PetType petType)
        {
            var bloodType = RetrieveBloodType(name, petType);
            if (bloodType != null)
                return bloodType;

            bloodType = new BloodType(name, petType);
            saveAs(bloodType);
            return bloodType;
        }
        public BloodType RetrieveBloodType(int bloodTypeId)
        {
            return _entities.Single<BloodType>(b => b.Id == bloodTypeId);
        }
        public BloodType RetrieveBloodType(string bloodTypeName, PetType petType)
        {
            return _entities.Single<BloodType>(b => b.Name == bloodTypeName && b.PetType == petType);
        }
        public List<BloodType> RetrieveBloodTypes(PetType petType)
        {
            return _entities.Get<BloodType>(b => b.PetType == petType).ToList();
        }
        public BloodType UpdateBloodType(int bloodTypeId, BloodType bloodTypeData)
        {
            if (bloodTypeExists(bloodTypeId))
            {
                var existingBloodType = RetrieveBloodType(bloodTypeId);
                existingBloodType = _mapper.Map<BloodType, BloodType>(bloodTypeData, existingBloodType);
                save(existingBloodType);
                return existingBloodType;
            }
            return null;
        }
        #endregion

        #region Helpers

        private bool breedExists(int breedId)
        {
            return _entities.Exists<Breed>(o => o.Id == breedId);
        }

        private bool bloodTypeExists(int bloodTypeId)
        {
            return _entities.Exists<BloodType>(o => o.Id == bloodTypeId);
        }        
        
        private T save<T>(T data) where T : class
        {
            _entities.Update(data);
            _entities.Save();
            return data;
        }

        private T saveAs<T>(T data) where T : class
        {
            _entities.Create(data); 
            _entities.Save();
            return data;
        }

        #endregion

    }
}