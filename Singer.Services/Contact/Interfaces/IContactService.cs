using System.Threading.Tasks;
using Singer.Domain;

namespace Singer.Services
{
    public interface IContactService
    {
        Task<bool> SaveContactDetailsAndConfirm(ContactEnquiry enquiryData, string emailContent, string origin);
    }
}