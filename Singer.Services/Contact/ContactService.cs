using System.Threading.Tasks;
using Microsoft.Extensions.Configuration;
using Cenozoic.Infrastructure;
using Cenozoic.Infrastructure.Communication;
using Singer.Domain;

namespace Singer.Services
{
    public class ContactService : IContactService
    {

        private readonly IWriteEntities _entities;
        private readonly IEmailService _emailServices;
        private readonly IConfiguration _configuration;

        public ContactService(IWriteEntities entities, IEmailService emailServices, IConfiguration configuration)
        {
            this._entities = entities;
            this._emailServices = emailServices;
            this._configuration = configuration;
        }
        public async Task<bool> SaveContactDetailsAndConfirm(ContactEnquiry enquiryData, string emailContent, string origin)
        {

            _entities.Create(enquiryData);
            _entities.Save();
            await _emailServices.Send(_configuration["Mandrill:FromEmail_" + origin], "Enquiry : " + enquiryData.Email, emailContent, origin);
            return true;
        }
    }
}