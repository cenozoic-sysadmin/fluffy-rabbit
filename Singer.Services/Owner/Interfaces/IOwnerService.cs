using System;
using System.Threading.Tasks;
using Singer.Domain;
using System.Collections.Generic;

namespace Singer.Services 
{
    public interface IOwnerService
    {
        Owner CreateOwner(Owner ownerData);
        bool EmailTaken(string email);
        Owner RetrieveOwner(int ownerId);
        Owner RetrieveOwner(Guid ownerToken);
        Owner UpdateOwner(int ownerId, Owner ownerData);

        List<Pet> RetrievePets(int ownerId);
        Pet RetrievePet(int ownerId, int petId);
        void UpdatePet(int ownerId, Pet petData);


        Task<bool> ReferToFriends(int ownerId, List<string> referralEmails, string emailContent, string origin);
    }
}