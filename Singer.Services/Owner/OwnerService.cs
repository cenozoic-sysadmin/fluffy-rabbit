using System;
using System.Threading.Tasks;
using AutoMapper;
using Singer.Domain;
using System.Collections.Generic;
using System.Linq;
using Cenozoic.Infrastructure;
using Cenozoic.Infrastructure.Communication;

namespace Singer.Services 
{
    public class OwnerService : IOwnerService
    {
        private readonly IWriteEntities _entities;
        private readonly IMapper _mapper;
        private readonly IEmailService _emailServices;

        public OwnerService(IWriteEntities entities, IMapper mapper, IEmailService emailServices)
        {
            this._entities = entities;
            this._mapper = mapper;
            this._emailServices = emailServices;
        }

        #region Owner Operations
        public Owner CreateOwner(Owner ownerData)
        {
            if (EmailTaken(ownerData.Email))
                return null;
            ownerData.Token = Guid.NewGuid();
            saveAs(ownerData);
            return ownerData;
        }

        public bool EmailTaken(string email)
        {
            return _entities.Exists<Owner>(o => o.Email == email);
        }

        public Owner RetrieveOwner(int ownerId)
        {
            if (ownerExists(ownerId))
                return _entities.Single<Owner>(o => o.Id == ownerId);

            return null;
        }

        public Owner RetrieveOwner(Guid ownerToken)
        {
            return _entities.Single<Owner>(o => o.Token == ownerToken);
        }

        public Owner UpdateOwner(int ownerId, Owner ownerData)
        {
            if (ownerExists(ownerId))
            {
                var existingOwner = RetrieveOwner(ownerId);
                existingOwner = _mapper.Map<Owner, Owner>(ownerData, existingOwner);
                save(existingOwner);
                return existingOwner;
            }
            return null;
        }

        public async Task<bool> ReferToFriends(int ownerId, List<string> referralEmails, string emailContent, string origin)
        {
            if (ownerExists(ownerId))
            {
                var existingOwner = RetrieveOwner(ownerId);
                for (var i = 0; i < referralEmails.Count && i < 3; i++)
                {
                    var ownerReferral = new OwnerReferral(ownerId, referralEmails[i]);
                    if (existingOwner.AssignReferral(ownerReferral))
                        await _emailServices.Send(referralEmails[i], "You have been invited to register your pet with " + origin, emailContent, origin);
                }
                save(existingOwner);
                return true;
            }
            return false;
        }

        #endregion

        #region Pet Operations

        public List<Pet> RetrievePets(int ownerId) {
            return _entities.Get<Pet>(p => p.OwnerId == ownerId).ToList();
        }

        public Pet RetrievePet(int ownerId, int petId) 
        {
            return _entities.Single<Pet>(p => p.Id == petId && p.OwnerId == ownerId);
        }

        public void UpdatePet(int ownerId, Pet petData) 
        {
            var pet = RetrievePet(ownerId, petData.Id);
            if (pet == null)
                return;
            pet = _mapper.Map(petData, pet);
            saveAs<Pet>(pet);
        }

        #endregion

        #region Helpers

        private bool ownerExists(int ownerId)
        {
            return _entities.Exists<Owner>(o => o.Id == ownerId);
        }
        
        private void save<T>(T data) where T : class
        {
            _entities.Update(data);
            _entities.Save();
        }

        private void saveAs<T>(T data) where T : class
        {
            _entities.Create(data);
            _entities.Save();
        }

        #endregion



    }
}