using Newtonsoft.Json;
using System;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;
using System.Collections.Generic;
using Microsoft.Extensions.Configuration;
using Cenozoic.Infrastructure.Caching;

namespace Cenozoic.Spatial
{
    public class GooglePlaceService : ISpatialService
    {
        private readonly ICacheService _cacheServices;
        private readonly IConfiguration _configuration;

        public GooglePlaceService(ICacheService cacheServices, IConfiguration configuration)
        {
            this._cacheServices = cacheServices;
            this._configuration = configuration;
        }

        #region Implementations

        public async Task<List<AutoCompleteDetails>> GetSpatialAutoComplete(string input)
        {
            if (string.IsNullOrEmpty(input) || input.Length < 3)
                return new List<AutoCompleteDetails>();
            return await _cacheServices.GetOrAddAsync("GooglePlace_" + input, () => this.GetGoogleAutoComplete(input), DateTimeOffset.Now.AddDays(1), null, null);
        }

        public async Task<LatLng> GetSpatialLatLng(string spatialId)
        {
            if (string.IsNullOrEmpty(spatialId))
                return new LatLng();
            return await _cacheServices.GetOrAdd(spatialId, () => this.GetGooglePlace(spatialId), DateTimeOffset.Now.AddDays(1), null, null);
        }

        #endregion

        #region Helpers

        private async Task<List<AutoCompleteDetails>> GetGoogleAutoComplete(string input)
		{
			var http = new HttpClient();
			var url = String.Format("https://maps.googleapis.com/maps/api/place/autocomplete/json?input={0}&types=address&components=country:au&key=" + _configuration["Google:Place-Key"], input);
			var response = await http.GetAsync(url);
			var jsonResponseString = await response.Content.ReadAsStringAsync();

			return JsonConvert.DeserializeObject<PlaceAutoCompleteRootobject>(jsonResponseString).predictions.Select(p =>
			{
				AutoCompleteDetails sd = new AutoCompleteDetails();
				sd.FullAddress = p.description;
				sd.SpatialId = p.place_id;
				return sd;
			}).ToList();
		}

        private async Task<LatLng> GetGooglePlace(string placeID)
		{
			var http = new HttpClient();
			var url = String.Format("https://maps.googleapis.com/maps/api/place/details/json?placeid={0}&key=" + _configuration["Google:Place-Key"], placeID);
			var response = await http.GetAsync(url);
			var jsonResponseString = await response.Content.ReadAsStringAsync();

			PlaceDetailsRootobject jsonRoot = JsonConvert.DeserializeObject<PlaceDetailsRootobject>(jsonResponseString);
			LatLng ll = new LatLng();
			ll.Lat = jsonRoot.result.geometry.location.lat;
			ll.Lng = jsonRoot.result.geometry.location.lng;

			return ll;
		}

        #endregion
    }
}