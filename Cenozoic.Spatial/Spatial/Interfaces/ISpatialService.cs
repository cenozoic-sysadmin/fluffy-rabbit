using System.Threading.Tasks;
using System.Collections.Generic;

namespace Cenozoic.Spatial 
{
    public interface ISpatialService
    {
        Task<List<AutoCompleteDetails>> GetSpatialAutoComplete(string input);
        Task<LatLng> GetSpatialLatLng(string spatialId);
    }
}