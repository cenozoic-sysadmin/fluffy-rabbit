
namespace Cenozoic.Spatial
{
    public class LatLng
    {
        public float Lat {get;set;}
        public float Lng {get;set;}
    }
    
}