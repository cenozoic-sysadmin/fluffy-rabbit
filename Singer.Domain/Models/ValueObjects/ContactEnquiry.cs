using System;
using System.ComponentModel.DataAnnotations.Schema;

namespace Singer.Domain
{
    [Table("ContactEnquiry")]
    public class ContactEnquiry
    {
        public int Id {get;set;}
        public string FirstName {get;set;}
        public string LastName {get;set;}
        public string Email {get;set;}
        public string Reason {get;set;}
        public string Message {get;set;}
        public string Origin {get;set;}
        public DateTime CreatedAt {get;set;}

        protected ContactEnquiry()
        {
            // EF
            this.CreatedAt = DateTime.Now;
        }

        public ContactEnquiry(string firstName, string lastName, string email, string reason, string message, string origin)
        {
            this.FirstName = firstName;
            this.LastName = lastName;
            this.Email = email;
            this.Reason = reason;
            this.Message = message;
            this.Origin = origin;
            this.CreatedAt = DateTime.Now;
        }
       
    }
}
