public enum PetSize
{ 
    Toy = 0,
    Small = 1,
    Medium = 2,
    Large = 3,
    XLarge = 4
}