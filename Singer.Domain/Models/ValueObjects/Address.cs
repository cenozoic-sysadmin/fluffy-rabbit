// EF 2.2 supports spatial models, but does not support scaffolding.
// https://blogs.msdn.microsoft.com/dotnet/2018/09/12/announcing-entity-framework-core-2-2-preview-2/
// $ dotnet add package Microsoft.EntityFrameworkCore.SqlServer -v 2.2.0-preview2-35157
// $ dotnet add package Microsoft.EntityFrameworkCore.SqlServer.NetTopologySuite -v 2.2.0-preview2-35157

// using NetTopologySuite.Geometries;

namespace Singer.Domain
{
	public class Address
	{
		public string Suburb { get; set; }
		public string State { get; set; }
		public string Postcode { get; set; }
		public string FullAddress { get; set; }
		public decimal Latitude { get; set; }
		public decimal Longitude { get; set; }
		public string SpatialId { get; set; }
        // public Point GeoPoint {get;set;}
	}
}
