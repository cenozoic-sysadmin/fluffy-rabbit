using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;



namespace Singer.Domain
{
    [Table("OwnerReferral")]
    public class OwnerReferral
    {
        public int Id {get;set;}
        public int OwnerId {get;set;}
        public Owner Owner {get;set;}
        public string Email {get;set;}
        public DateTime CreatedAt {get;set;}

        protected OwnerReferral()
        {
            // EF
            this.CreatedAt = DateTime.Now;
        }

        public OwnerReferral(int ownerId, string email)
        {
            this.CreatedAt = DateTime.Now;
            this.OwnerId = ownerId;
            this.Email = email;
        }
       
    }
}
