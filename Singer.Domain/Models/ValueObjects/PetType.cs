using System;

namespace Singer.Domain
{
    public enum PetType
    {
        Dog = 0,
        Cat = 1
    }
}
