using System;
using System.ComponentModel.DataAnnotations.Schema;
namespace Singer.Domain
{
    [Table("BloodType")]
    public class BloodType
    {
        public int Id {get;set;}
        public PetType PetType {get;set;}
        public string Name {get;set;}
        public DateTime CreatedAt {get;set;}

        protected BloodType () 
        {
            // EF
        }

        public BloodType(string name, PetType petType)
        {
            this.Name = name;
            this.PetType = petType;
        }
    }
}
