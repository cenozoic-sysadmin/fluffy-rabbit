using System;

namespace Singer.Domain
{
    public enum Gender
    {
        Male = 0,
        Female = 1
    }
}
