using System;
using System.ComponentModel.DataAnnotations.Schema;
namespace Singer.Domain
{
    [Table("PetBreed")]
    public class PetBreed
    {
        public int Id {get;set;}
        public int PetId {get;set;}
        public Pet Pet {get;set;}
        public int BreedId {get;set;}
        public int Order {get;set;}
        public DateTime CreatedAt {get;set;}

        protected PetBreed()
        {
            this.CreatedAt = DateTime.Now;
        }

        public PetBreed(int petId, int breedId)
        {
            this.PetId = petId;
            this.BreedId = breedId;
            this.CreatedAt = DateTime.Now;
        }
    }
}
