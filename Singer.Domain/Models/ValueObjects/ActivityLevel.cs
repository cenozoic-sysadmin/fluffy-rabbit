namespace Singer.Domain
{
	public enum ActivityLevel
	{
		ExtremelyInactive = 0,
        Sedentary = 1,
        ModeratelyActive = 2,
        VigorouslyActive = 3,
        ExtremelyActive = 4
	}
}
