namespace Singer.Domain
{
	public enum AgeUnit
	{
		Month = 0,
        Year = 1,
	}
}
