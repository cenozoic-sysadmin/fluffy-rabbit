﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;



namespace Singer.Domain
{
    [Table("Owner")]
    public class Owner
    {
        public int Id {get;set;}
        public string FirstName {get;set;}
        public string LastName {get;set;}
        public Gender Gender {get;set;}
        public DateTime DoB {get;set;}
        public string Mobile {get;set;}
        public string Email {get;set;}
        public Address Address {get;set;}
        public virtual ICollection<Pet> Pets {get;set;}
        public virtual ICollection<OwnerReferral> Referrals {get;set;}
        public DateTime CreatedAt {get;set;}
        public DateTime UpdatedAt {get;set;}
        public Guid Token {get;set;}

        public string Origin {get;set;}

        protected Owner() 
        {
            // EF
            this.Address = new Address();
            this.CreatedAt = DateTime.Now;
            this.UpdatedAt = this.CreatedAt;
            this.Pets = new List<Pet>();
            this.Referrals = new List<OwnerReferral>();
            this.Token = Guid.NewGuid();
        }

        public Owner(string loginId = "")
        {
            this.Address = new Address();
            this.CreatedAt = DateTime.Now;
            this.UpdatedAt = this.CreatedAt;
            this.Pets = new List<Pet>();
            this.Referrals = new List<OwnerReferral>();
            this.Token = Guid.NewGuid();
        }

        #region Domain logic

        public bool AssignPet(Pet pet)
        {
            if (petAssigned(pet)) 
                return false;

            this.Pets.Add(pet);
            return true;
        }

        public bool AssignReferral(OwnerReferral referral)
        {
            if (alreadyReferred(referral))
                return false;
            this.Referrals.Add(referral);
            return true;
        }

        #endregion

        #region Helpers

        private bool petAssigned(Pet pet)
        {
            return this.Pets.Where(p => p.Id > 0 && p.Id == pet.Id).Count() > 0;
        }

        private bool alreadyReferred(OwnerReferral referral)
        {
            return this.Referrals.Where(p => p.Email == referral.Email).Count() > 0;
        }

        #endregion
    }
}
