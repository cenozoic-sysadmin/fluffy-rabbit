using System;
using System.Linq;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;

namespace Singer.Domain
{
    [Table("Pet")]
    public class Pet
    {
        public int Id {get;set;}
        public int OwnerId {get;set;}
        public string Name {get;set;}
        public Gender Gender {get;set;}
        public PetType Type {get;set;}
        public BloodType BloodType {get;set;}
        public ActivityLevel ActivityLevel {get;set;}
        public PetSize Size {get;set;}
        public ICollection<PetBreed> PetBreeds {get;set;}
        public DateTime DoB {get;set;}
        public decimal Weight {get;set;}
        public string ProfilePicture {get;set;}
        public DateTime CreatedAt {get;set;}
        public DateTime UpdatedAt {get;set;}

        protected Pet()
        {
            // EF
            this.CreatedAt = DateTime.Now;
            this.UpdatedAt = this.CreatedAt;
            this.PetBreeds = new List<PetBreed>();
        }

        public Pet(int ownerId)
        {
            this.CreatedAt = DateTime.Now;
            this.UpdatedAt = this.CreatedAt;
            this.PetBreeds = new List<PetBreed>();
            this.OwnerId = ownerId;
        }

        #region Domain logic
        
        public bool AssignBreed(Breed breed)
        {
            if (breedAssigned(breed))
                return false;
                
            var petBreed = new PetBreed(petId: this.Id, breedId: breed.Id);
            this.PetBreeds.Add(petBreed);
            return true;
        }

        public bool AssignBloodType(BloodType bloodType)
        {
            if (bloodType.PetType != this.Type)
                return false;

            this.BloodType = bloodType;
            return true;
        }

        public bool AssignProfilePicture(string path)
        {
            this.ProfilePicture = path;
            return true;
        }

        #endregion

        #region Helpers
        
        private bool breedAssigned(Breed breed)
        {
            return this.PetBreeds.Where(pb => pb.BreedId == breed.Id).Count() > 0;
        }
        
        #endregion


    }

    
}
