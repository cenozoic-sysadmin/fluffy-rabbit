using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;

namespace Singer.Domain
{
    [Table("Breed")]
    public class Breed
    {
        public int Id {get;set;}
        public string Name {get;set;}
        public PetType PetType {get;set;}
        public bool Approved {get;set;}

        protected Breed()
        {
            // EF
        }

        public Breed(string name, PetType petType)
        {
            this.Name = name;
            this.PetType = petType;
            this.Approved = false;
        }
    }
}